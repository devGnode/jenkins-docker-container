#!/bin/sh

#
# update CA-CERT
#
update-ca-certificates
#
# Docker : /var/run/docker.sock
#
usermod -aG docker jenkins
usermod -aG root jenkins
chmod ugo+rw /var/run/docker.sock
#
# git config
#
git config --global user.email "jenkins@provider.tld"
git config --global user.name "jenkins-bot"
#
# DEFINE JAVA_HOME VAR
#
if [ -z "${JAVA_HOME}" ]
then
  export JAVA_HOME=$( dirname `update-alternatives --list java` )
  export PATH=$PATH:$JAVA_HOME
fi
#
# https configuration
#
export JENKINS_ARGS="--webroot=/var/cache/jenkins/war"
if [ ! -z "${HTTPS_CERTS}" ]
then
  export JENKINS_ARGS="$JENKINS_ARGS --httpPort=-1 --httpsPort=8443 --httpsCertificate=${HTTPS_CERTS} --httpsPrivateKey=${HTTPS_KEY}"
else
 export JENKINS_ARGS="$JENKINS_ARGS --httpPort=-${HTTP_PORT}"
fi

#
# volumes permissions
#
chown -R $JENKINS_UID:$JENKINS_UID /var/lib/jenkins
#
# RUN
# to see : run from a daemon-start-stop in java
#
/etc/init.d/jenkins start
sleep infinity