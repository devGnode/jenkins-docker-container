FROM debian
MAINTAINER madevincre <madevincre@zenity.fr>

COPY docker-entrypoint.sh /sbin/docker-entrypoint.sh
COPY jenkins/daemon.json /etc/docker/daemon.json
#
# add certificat
#
RUN mkdir -p /certs
COPY CERTS/jenkins.local/CERTS/jenkins.local.crt /certs/jenkins.local.crt
COPY CERTS/jenkins.local/CERTS/jenkins.local.key /certs/jenkins.local.key
#
# PORTS
#
EXPOSE 8080
EXPOSE 8443
#
# Environment var
#
ENV JAVA_HOME="" \
    JENKINS_UID=jenkins \
    HTTP_HOST=0.0.0.0 \
    HTTP_PORT=8080 \
    HTTPS_CERTS="" \
    HTTPS_KEY="" \
    HTTPS_CACERT=""

RUN apt-get update -y \
        && apt-get install -y dialog \
        apt-utils \
        sudo  \
        apt-transport-https \
        ca-certificates \
        curl \
        wget \
        dnsutils \
        gnupg \
        gnupg2 \
        && apt-get update -y

#
# JAVA
#
RUN apt-get install -y openjdk-11-jdk

# KEY
RUN apt-key adv --fetch-keys https://pkg.jenkins.io/debian-stable/jenkins.io.key

# JENKINS
RUN echo 'deb https://pkg.jenkins.io/debian-stable binary/' >> /etc/apt/sources.list \
        && apt-get -y update \
        && apt-get install -y jenkins \
        git \
        ssh \
        ssh-client \
        docker-compose \
        nmap \
        dnsutils

#
# remove docker helper credential X11
#
RUN apt-get remove -y golang-docker-credential-helpers
#
#
COPY ./jenkins/jenkins /etc/default/jenkins
RUN chmod +x /sbin/docker-entrypoint.sh
#
# Volume
#
VOLUME ["/var/log/jenkins"]
VOLUME ["/var/lib/jenkins"]
#
# jenkins user
#
CMD ["/sbin/docker-entrypoint.sh"]