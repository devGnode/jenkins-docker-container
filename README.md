# jenkins-service

<img src="https://img.shields.io/badge/Server-DevOps-green"> <img src="https://img.shields.io/badge/vpod-Docker-blue"> <img src="https://img.shields.io/badge/Remote-172.31.1.80-red">

IMG_NAME : `jenkins-debian`

POD_NAME : `vpod-deploy-jenkins`

Docker : `jenkins-debian:1.0.0` 


### Volume

- `/usr/local/share/ca-certificates` : CA-CERT
- `/certs` : SERVER-CERT : `${DIR}/CERTS`
- `/var/log/jenkins` : `${DIR}/logs`
- `/var/lib/jenkins` : `/var/jenkins_home`
- `/var/run/docker.sock` : docker service

## Jenkins : Docker directory 

- `etc/docker` : installation directory
- `/var/run/docker.sock` : docker socket to bind with container
- `/etc/docker`

> --volume /var/run/docker.sock:/var/run/docker.sock

entrypoint :

````shell
usermod -aG docker jenkins
usermod -aG root jenkins
chmod ugo+rw /var/run/docker.sock
````

### setup

- make.sh

````shell
chmod +x make.sh
./make.sh
````  

- deploy.sh

> --nobuild : no re-rm and re-build image
> 
> --nokill : after build docker has failed to launch container run with this
> 
> --jenkinshome
> 
> --downjenkinshome
> 
> --savejenkinshome

- deploy new image

````shell
deploy.sh --jenkinshome=/var
````

- deploy without kill old container

````shell
deploy.sh --nokill=true --jenkinshome=/var
````

- re-deploy new container

````shell
deploy.sh --nobuild=true --jenkinshome=/var
````

- re-deploy new container with update jenkins_home directory

````shell
deploy.sh --nobuild=true --jenkinshome=/var --downjenkinshome=s1.storage../jenkins-service
````

- re-deploy new container with update jenkins_home directory with save old jenkins_home

````shell
deploy.sh --nobuild=true --jenkinshome=/var --downjenkinshome=s1.storage../jenkins-service --savejenkinshome=true
````