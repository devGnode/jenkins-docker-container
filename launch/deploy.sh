#!/bin/sh

DIR=$( dirname $PWD )
VERSION=$( cat $DIR/pod_version )

echo "deploy version ${VERSION}"

# POD_CONFIG
IMG_NAME=jenkins-debian
POD_NAME=vpod-deploy-jenkins
GTE_NAME=jenkins-net
#
REDIRECT_PORT_HTTP=80:8080
REDIRECT_PORT_HTTPS=443:8443

HTTPS="0"
CERT_VOLUME=${DIR}/CERTS
#
#
NO_BUILD=
NO_KILL=
#
#
JENKINS_SAVE_HOME=
DOWN_JENKINS_HOME=
JENKINS_HOMEA=${DIR}

for arg in "$@"
do
    case $arg in
        -n=*|--name=*)
        IMG_NAME="${arg#*=}"
        shift
        ;;
        -p=*|--podname=*)
        POD_NAME="${arg#*=}"
        shift
        ;;
        -n=*|--network=*)
        GTE_NAME="${arg#*=}"
        shift
        ;;
        -b=*|--nobuild=*)
        NO_BUILD="${arg#*=}"
        shift
        ;;
        -v=*|--version=*)
        VERSION="${arg#*=}"
        shift
        ;;
        -k=*|--nokill=*)
        NO_KILL="${arg#*=}"
        shift
        ;;
        -r=*|--downjenkinshome=*)
        DOWN_JENKINS_HOME="${arg#*=}"
        shift
        ;;
        -h=*|--jenkinshome=*)
        JENKINS_HOMEA="${arg#*=}"
        shift
        ;;
        --savejenkinshome=*)
        JENKINS_SAVE_HOME="${arg#*=}"
        shift
        ;;
        *)
        null=$1
        shift
        ;;
    esac
done

port=`echo ${REDIRECT_PORT} | cut -d: -f 2`

if [ -d "$JENKINS_HOMEA/jenkins_home" ] && [ ! -z "$JENKINS_SAVE_HOME" ]
then
  echo "jenkins_home saver"
  sh snap-jenkins-workspace.sh $JENKINS_HOMEA
fi

# CHECK NETWORK BRIDGE
exists=`docker network ls | grep "${GTE_NAME}"`
if [ -z "${exists}" ]
then
	docker network create ${GTE_NAME}
fi

if [ -z "$NO_KILL" ]
then

  # Kill PS
  pida=$( docker ps | grep ${POD_NAME}-${VERSION} | cut -d ' ' -f 1 )
  if [ "${#pida}" -eq "12" ]
  then
    echo "Kill pod : ${POD_NAME}"
    docker stop ${POD_NAME}-${VERSION}
    docker rm ${POD_NAME}-${VERSION}
  fi

fi

if [ -z "$NO_BUILD" ]
then
  # img
  img=$( docker image ls ${IMG_NAME}:${VERSION} | grep ${IMG_NAME} )
  if [ ! -z "${img}"  ]
  then
    echo "Remove old pod image : ${POD_NAME}:${VERSION}"
    docker image rm ${IMG_NAME}:${VERSION}
  fi

  echo "Build ..."
  docker build -t ${IMG_NAME}:${VERSION} ${DIR}

fi

if [ ! -z "$DOWN_JENKINS_HOME" ]
then
  wget https://${DOWN_JENKINS_HOME}/jenkins_home-${VERSION}/jenkins_home-${VERSION}.tar.gz -O ${DIR}/jenkins_home.tar.gz || exit 1
fi

echo "jenkins home"
if [  -f "${DIR}/jenkins_home.tar.gz" ]
then
    tar -zxvf ${DIR}/jenkins_home.tar.gz
    rm -rf ${JENKINS_HOMEA}/jenkins_home
    cp -rf jenkins_home ${JENKINS_HOMEA}/
    rm -rf jenkins_home ${DIR}/jenkins_home.tar.gz
fi

ARGV=
PORT=$REDIRECT_PORT_HTTP
if [ "$HTTPS" -eq "1" ]
then
  ARGV="-e HTTPS_CERTS=/certs/jenkins.local.crt -e HTTPS_CERTS=/certs/jenkins.local.crt --volume /usr/local/share/ca-certificates:/usr/local/share/ca-certificates"
  PORT=$REDIRECT_PORT_HTTPS
fi

docker run -tid --name ${POD_NAME}-${VERSION} "${ARGV}" \
  --privileged \
  --restart=always \
  --network ${GTE_NAME} \
  --expose 8443 \
  -p ${PORT} \
  --volume /var/run/docker.sock:/var/run/docker.sock \
  --volume ${JENKINS_HOMEA}/jenkins_home:/var/lib/jenkins \
  --volume /var/log/jenkins:/var/log/jenkins \
  ${IMG_NAME}:${VERSION}
