#!/bin/sh
DIR=/var

for arg in "$@"
do
    case $arg in
        -o=*|--output=*)
        DIR="${arg#*=}"
        shift
        ;;
        *)
        null=$1
        shift
        ;;
    esac
done

timestamp=$( date +%Y-%m-%d )
tar -cvf /tmp/jenkins_home-${timestamp}.tar.gz ${DIR}/jenkins_home/

if [ -d "/media/registry" ]
then
  mv -f /tmp/jenkins_home-${timestamp}.tar.gz /media/registry/jenkins_home-${timestamp}.tar.gz
  exit 0
fi

exit 1